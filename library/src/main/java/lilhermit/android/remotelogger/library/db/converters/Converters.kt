package lilhermit.android.remotelogger.library.db.converters

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import java.util.*


class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.run { Date(this) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun stringArrayToString(stringList: MutableList<String>?): String {
        return stringList?.run { Gson().toJson(this) } ?: "[]"
    }

    @TypeConverter
    fun stringToStringList(json: String?): List<String> {
        return json?.run {
            val stringListType = object : TypeToken<List<String>>() {}.type
            Gson().fromJson(this, stringListType) as List<String>
        } ?: mutableListOf<String>() as List<String>
    }

    @TypeConverter
    fun jsonElementToString(jsonElement: JsonElement?): String {
        return jsonElement?.toString() ?: "{}"
    }

    @TypeConverter
    fun stringToJsonElement(json: String): JsonElement {
        return JsonParser().parse(json)
    }
}