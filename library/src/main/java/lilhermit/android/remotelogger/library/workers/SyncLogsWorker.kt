package lilhermit.android.remotelogger.library.workers

import android.arch.persistence.room.Room
import android.content.Context
import androidx.work.Data
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.GsonBuilder
import lilhermit.android.remotelogger.library.BuildConfig
import lilhermit.android.remotelogger.library.Log
import lilhermit.android.remotelogger.library.api.ApiService
import lilhermit.android.remotelogger.library.db.LogEntryDao
import lilhermit.android.remotelogger.library.db.RemoteLoggerDatabase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal class SyncLogsWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    private var apiService: ApiService = buildApiService()
    private var logEntryDoa: LogEntryDao = Room.databaseBuilder(
        context.applicationContext, RemoteLoggerDatabase::class.java, "remote-logger.db"
    ).build().logEntryDao()

    override fun doWork(): Result {

        if (Log.getAccessToken() == null) {
            return ListenableWorker.Result.success()
        }

        val syncableLogs = logEntryDoa.getSyncable()
        if (!syncableLogs.isEmpty()) {

            try {


                val response = apiService.postLogs(syncableLogs).execute()
                return if (response.isSuccessful) {

                    syncableLogs.forEach { log ->
                        log.id?.let {
                            logEntryDoa.setSynced(it)
                        }
                    }
                    ListenableWorker.Result.success(Data.Builder().putInt("sync_log_count", syncableLogs.size).build())
                } else {
                    ListenableWorker.Result.retry()
                }
            } catch (exception: Exception) {
                android.util.Log.e("RemoteLogger", "SyncLogsWorker: Exception: ${exception.message}")
                return ListenableWorker.Result.retry()
            }
        }

        return ListenableWorker.Result.success()
    }

    private fun buildApiService(): ApiService {
        val httpClientBuilder = OkHttpClient.Builder()

        httpClientBuilder.addInterceptor { chain ->
            val ongoing = chain.request().newBuilder()
            ongoing.addHeader("Accept", "application/json")
            Log.getAccessToken()?.let {
                ongoing.addHeader("Authorization", "Bearer $it")
            }
            chain.proceed(ongoing.build())
        }

        // Add HttpLogging for requests
        if (BuildConfig.DEBUG) {
            val httpIntercept = HttpLoggingInterceptor()
            httpIntercept.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(httpIntercept)
        }

        return Retrofit.Builder()
            .baseUrl(Log.getApiUrl())
            .client(httpClientBuilder.build())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS")
                        .create()
                )
            )
            .build()
            .create<ApiService>(ApiService::class.java)
    }
}