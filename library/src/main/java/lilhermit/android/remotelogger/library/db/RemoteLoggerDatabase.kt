package lilhermit.android.remotelogger.library.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import lilhermit.android.remotelogger.library.LogEntry
import lilhermit.android.remotelogger.library.db.converters.Converters

@Database(entities = [LogEntry::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
internal abstract class RemoteLoggerDatabase : RoomDatabase() {

    abstract fun logEntryDao(): LogEntryDao
}