package lilhermit.android.remotelogger.library

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.JsonElement
import com.google.gson.annotations.Expose
import java.util.*

@Entity(tableName = "log_entries")
internal class LogEntry {

    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    @Expose
    var text: String? = null
    @Expose
    var uid: String? = null
    @Expose
    var logged_at = Date()
    @Expose
    var expiry: String? = null
    @Expose
    var level = Log.INFO
    @Expose
    var platform = "Android"
    @Expose
    var tags: MutableList<String> = mutableListOf()
    @Expose
    var data: JsonElement? = null

    var sync_stamp: Date? = null
}