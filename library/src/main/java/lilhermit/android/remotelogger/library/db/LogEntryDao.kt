package lilhermit.android.remotelogger.library.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import lilhermit.android.remotelogger.library.LogEntry


@Dao
internal interface LogEntryDao {

    @Insert(onConflict = REPLACE)
    fun insert(logEntry: LogEntry)

    @Update()
    fun update(logEntry: LogEntry)

    @Query("SELECT * FROM log_entries")
    fun getAll(): List<LogEntry>

    @Query("SELECT * FROM log_entries WHERE sync_stamp is null")
    fun getSyncable(): List<LogEntry>


    @Query("UPDATE log_entries set sync_stamp = strftime('%s',datetime('now')) * 1000 where id = :id")
    fun setSynced(id: Int)

    @Query("DELETE FROM log_entries WHERE sync_stamp is not null and sync_stamp < strftime('%s',datetime('now','-3 days')) * 1000;")
    fun purgeSyncedLogEntries()

    @Query("DELETE FROM log_entries WHERE sync_stamp is null and logged_at < strftime('%s',datetime('now','-14 days')) * 1000;")
    fun purgeUnsyncedLogEntries()
}