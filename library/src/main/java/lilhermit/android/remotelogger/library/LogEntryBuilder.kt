package lilhermit.android.remotelogger.library

import com.google.gson.Gson
import java.util.*

class LogEntryBuilder {

    private var logEntry = LogEntry()
    private var includeGlobalTags = true
    private var includeGlobalExpiry = true

    fun setLevel(level: Int): LogEntryBuilder {
        logEntry.level = level
        return this
    }

    fun setText(text: String): LogEntryBuilder {
        logEntry.text = text
        return this
    }

    fun setExpiry(expiry: Date): LogEntryBuilder {
        logEntry.expiry = expiry.toString()
        return this
    }

    fun setExpiry(relative: String): LogEntryBuilder {
        logEntry.expiry = relative
        return this
    }

    fun setTags(tags: List<String>): LogEntryBuilder {
        logEntry.tags = tags.toMutableList()
        return this
    }

    fun setTags(vararg tags: String): LogEntryBuilder {
        logEntry.tags = tags.toMutableList()
        return this
    }

    fun addTag(tag: String): LogEntryBuilder {
        logEntry.tags.add(tag)
        return this
    }

    fun setData(any: Any): LogEntryBuilder {
        logEntry.data = Gson().toJsonTree(any)
        return this
    }

    fun setUid(uid: String): LogEntryBuilder {
        logEntry.uid = uid
        return this
    }

    fun setIncludeGlobalTags(include: Boolean): LogEntryBuilder {
        includeGlobalTags = include
        return this
    }

    fun setIncludeGlobalExpiry(include: Boolean): LogEntryBuilder {
        includeGlobalExpiry = include
        return this
    }

    fun send() {

        if (logEntry.text == null && logEntry.data == null) {
            throw RuntimeException("Text and Data cannot be null")
        }

        // Add the global tags if required
        if (includeGlobalTags) {
            logEntry.tags.addAll(Log.getTags())
        }

        // Add the global expiry if required and no expiry is set on the LogEntry
        val globalExpiry = Log.getExpiry()
        if (includeGlobalExpiry && logEntry.expiry == null && globalExpiry != null) {
            logEntry.expiry = globalExpiry
        }

        // Add the global uid if required
        val globalUid = Log.getUid()
        if (globalUid != null && logEntry.uid == null) {
            logEntry.uid = globalUid
        }

        Log.log(logEntry)
    }
}