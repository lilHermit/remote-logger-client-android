package lilhermit.android.remotelogger.library.api


import lilhermit.android.remotelogger.library.LogEntry
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

internal interface ApiService {

    /**
     * Post logs
     *
     * @POST declares an HTTP POST request
     */
    @POST("logs")
    fun postLogs(@Body logEntries: List<LogEntry>): Call<ApiResponse>

}