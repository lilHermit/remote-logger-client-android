package lilhermit.android.remotelogger.library

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Room
import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import androidx.work.*
import lilhermit.android.remotelogger.library.db.RemoteLoggerDatabase
import lilhermit.android.remotelogger.library.workers.PurgeLogsWorker
import lilhermit.android.remotelogger.library.workers.SyncLogsWorker
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class Log {

    companion object {

        const val VERBOSE = android.util.Log.VERBOSE
        const val INFO = android.util.Log.INFO
        const val DEBUG = android.util.Log.DEBUG
        const val WARN = android.util.Log.WARN
        const val ERROR = android.util.Log.ERROR

        const val LOG_NONE = 0
        const val LOG_LOCAL_ONLY = 1
        const val LOG_REMOTE_ONLY = 2
        const val LOG_BOTH = 3

        private val logLevelMap: HashMap<Int, Int> = hashMapOf(
            VERBOSE to LOG_LOCAL_ONLY,
            INFO to LOG_LOCAL_ONLY,
            DEBUG to LOG_BOTH,
            WARN to LOG_LOCAL_ONLY,
            ERROR to LOG_BOTH
        )

        private var logtag: String? = null
        private var accessToken: String? = null
        private var apiUrl: String = "https://api.remote-logger.lilhermit.co.uk"
        private var remoteEnabled = false
        private var db: RemoteLoggerDatabase? = null
        private var singleThread = Executors.newSingleThreadExecutor()
        private var tags: MutableList<String> = mutableListOf()
        private var expiry: String? = null
        private var uid: String? = null
        private var repeatInterval: Long = 30
        private var repeatIntervalTimeUnit: TimeUnit = TimeUnit.MINUTES

        fun init(logtag: String, accessToken: String? = null): Log.Companion {
            this.logtag = logtag
            setAccessToken(accessToken)
            return this
        }

        internal fun internalInit(context: Context?) {
            context?.run {

                try {
                    // Disable WorkManagerInitializer then run manually
                    val workManagerInitializer = ComponentName(this, "androidx.work.impl.WorkManagerInitializer")
                    packageManager.setComponentEnabledSetting(
                        workManagerInitializer,
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        0
                    )
                    WorkManager.initialize(context, Configuration.Builder().build())
                } catch (exception: Exception) {
                }

                db = Room.databaseBuilder(
                    applicationContext, RemoteLoggerDatabase::class.java, "remote-logger.db"
                ).build()
                setupSyncWorker()
                setupPurgeWorker()
            }
        }

        private fun setupPurgeWorker() {

            WorkManager.getInstance().run {
                val purgeLogsWorker = PeriodicWorkRequestBuilder<PurgeLogsWorker>(1, TimeUnit.DAYS)
                    .build()
                enqueueUniquePeriodicWork("purge_logs", ExistingPeriodicWorkPolicy.KEEP, purgeLogsWorker)
            }
        }

        private fun setupSyncWorker() {

            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val syncLogsWorker = PeriodicWorkRequestBuilder<SyncLogsWorker>(repeatInterval, repeatIntervalTimeUnit)
                .setConstraints(constraints)
                .build()

            WorkManager.getInstance().run {
                enqueueUniquePeriodicWork("sync_logs", ExistingPeriodicWorkPolicy.KEEP, syncLogsWorker)
            }
        }

        fun setLevelLogging(vararg pair: Pair<Int, Int>): Log.Companion {
            pair.forEach {
                logLevelMap[it.first] = it.second
            }
            return this
        }

        internal fun getAccessToken(): String? = accessToken
        internal fun getApiUrl(): String = apiUrl
        internal fun getExpiry() = expiry
        internal fun getUid() = uid
        internal fun getTags(): MutableList<String> = tags

        fun setApiUrl(url: String): Log.Companion {
            apiUrl = url
            return this
        }

        fun setSyncPeriod(repeatInterval: Long, repeatIntervalTimeUnit: TimeUnit): Log.Companion {
            this.repeatInterval = repeatInterval
            this.repeatIntervalTimeUnit = repeatIntervalTimeUnit
            setupSyncWorker()
            return this
        }

        fun setRemoteEnabled(enabled: Boolean): Log.Companion {
            remoteEnabled = enabled
            return this
        }

        fun setTags(tags: List<String>): Log.Companion {
            this.tags = tags.toMutableList()
            return this
        }

        fun setTags(vararg tags: String): Log.Companion {
            this.tags = tags.toMutableList()
            return this
        }

        fun addTags(tags: List<String>): Log.Companion {
            this.tags.addAll(tags)
            return this
        }

        fun addTag(tag: String): Log.Companion {
            this.tags.add(tag)
            return this
        }

        fun setExpiry(expiry: Date): Log.Companion {
            this.expiry = expiry.toString()
            return this
        }

        fun setExpiry(relative: String): Log.Companion {
            this.expiry = relative
            return this
        }

        fun setUid(uid: String): Log.Companion {
            this.uid = uid
            return this
        }

        private fun setAccessToken(accessToken: String?) {
            this.accessToken = accessToken
            remoteEnabled = accessToken != null
        }

        fun builder(): LogEntryBuilder = LogEntryBuilder()
        fun v(): LogEntryBuilder = LogEntryBuilder().setLevel(VERBOSE)
        fun w(): LogEntryBuilder = LogEntryBuilder().setLevel(WARN)
        fun i(): LogEntryBuilder = LogEntryBuilder().setLevel(INFO)
        fun d(): LogEntryBuilder = LogEntryBuilder().setLevel(DEBUG)
        fun e(): LogEntryBuilder = LogEntryBuilder().setLevel(ERROR)

        fun v(msg: String) {
            v().setText(msg).send()
        }

        fun w(msg: String) {
            w().setText(msg).send()
        }

        fun i(msg: String) {
            i().setText(msg).send()
        }

        fun d(msg: String) {
            d().setText(msg).send()
        }

        fun e(msg: String) {
            e().setText(msg).send()
        }

        internal fun log(logEntry: LogEntry) {

            // Log locally
            if (shouldLogLocal(logEntry)) {
                logLocally(logEntry)
            }

            // Log remotely
            if (shouldLogRemote(logEntry)) {
                logRemotely(logEntry)
            }
        }

        private fun logRemotely(logEntry: LogEntry) {

            if (accessToken != null) {
                singleThread.execute {
                    db?.logEntryDao()?.insert(logEntry)
                }
            }
        }

        private fun logLocally(logEntry: LogEntry) {
            checkLogtag()
            if (logEntry.text == null) {
                return
            }

            when (logEntry.level) {
                VERBOSE -> android.util.Log.v(logtag, logEntry.text)
                INFO -> android.util.Log.i(logtag, logEntry.text)
                WARN -> android.util.Log.w(logtag, logEntry.text)
                DEBUG -> android.util.Log.d(logtag, logEntry.text)
                ERROR -> android.util.Log.e(logtag, logEntry.text)
            }
        }

        private fun shouldLogLocal(logEntry: LogEntry): Boolean = when (logLevelMap[logEntry.level]) {
            LOG_BOTH, LOG_LOCAL_ONLY -> true
            else -> false
        }

        private fun shouldLogRemote(logEntry: LogEntry): Boolean {
            if (!remoteEnabled) {
                return false
            }

            return when (logLevelMap[logEntry.level]) {
                LOG_BOTH, LOG_REMOTE_ONLY -> true
                else -> false
            }
        }

        private fun checkLogtag() {
            if (logtag == null) {
                throw IllegalArgumentException("Logtag is not set please call Log.init")
            }
        }

        fun forceSync(): LiveData<WorkInfo> {

            val syncLogsWorker = OneTimeWorkRequestBuilder<SyncLogsWorker>()
                .setConstraints(
                    Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()
                )
                .build()

            WorkManager.getInstance().run {
                enqueue(syncLogsWorker)
                return getWorkInfoByIdLiveData(syncLogsWorker.id)
            }
        }
    }
}