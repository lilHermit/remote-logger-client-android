package lilhermit.android.remotelogger.library.api

class ApiResponse {

    var message: String = "Unparsable resource (Unknown error)"
    var code: Int = 200
}