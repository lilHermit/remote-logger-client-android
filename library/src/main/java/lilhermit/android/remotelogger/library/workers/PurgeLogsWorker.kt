package lilhermit.android.remotelogger.library.workers

import android.arch.persistence.room.Room
import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import lilhermit.android.remotelogger.library.db.LogEntryDao
import lilhermit.android.remotelogger.library.db.RemoteLoggerDatabase

internal class PurgeLogsWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    private var logEntryDoa: LogEntryDao = Room.databaseBuilder(
        context.applicationContext, RemoteLoggerDatabase::class.java, "remote-logger.db"
    ).build().logEntryDao()

    override fun doWork(): Result {

        logEntryDoa.purgeSyncedLogEntries()
        logEntryDoa.purgeUnsyncedLogEntries()

        return ListenableWorker.Result.success()
    }
}